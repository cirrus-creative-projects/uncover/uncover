// ? Set Variables used later in functions
const cloudinary = window.cloudinary
const cloudName = `uncover`
export const cloudinaryBaseUrl = `https://res.cloudinary.com/${cloudName}/image/upload`

// * Profile Image Upload Widget
// ? Create an upload widget, and open it. Then, return image details once completed.
export const uploadProfileImage = (userId) => {
  return new Promise((resolve, reject) => {
    cloudinary.openUploadWidget({
      cloud_name: cloudName,
      upload_preset: 'profile-image-upload',
      multiple: false,
      cropping: 'server',
      cropping_aspect_ratio: 1,
      folder: `users/${userId}/profiles`,
      stylesheet: 'https://widget.cloudinary.com/n/demo/118/themes/white.css'
    }, (error, result) => {
      if (error) reject(error)
      else resolve(result)
    })
  })
}

// * New Image Upload Widget
// ? Create an upload widget, and open it. Then, return image details once completed.
export const uploadImage = (userId) => {
  return new Promise((resolve, reject) => {
    cloudinary.openUploadWidget({
      cloud_name: cloudName,
      upload_preset: 'image-upload',
      multiple: true,
      folder: `users/${userId}/images`,
      stylesheet: 'https://widget.cloudinary.com/n/demo/118/themes/white.css'
    }, (error, result) => {
      if (error) reject(error)
      else resolve(result)
    })
  })
}
