import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
// * Import Apollo
import { createProvider } from './vue-apollo'

//* Import styles
import './assets/scss/app.scss'

//* Import Plugins
import Buefy from 'buefy'
import VueCloudinary from 'vue-cloudinary'

//* Setup and use plugins
Vue.use(Buefy, {
  defaultIconPack: 'fas'
})
Vue.use(VueCloudinary, {
  'cloud_name': 'uncover',
  'api_key': '442773932881126'
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  provide: createProvider().provide(),
  render: h => h(App)
}).$mount('#app')
