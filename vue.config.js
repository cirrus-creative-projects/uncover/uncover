module.exports = {
  lintOnSave: true,
  pwa: {
    name: 'Uncover | Photography',
    themeColor: '#6C91BF',
    msTileColor: '#0C0904'
  }
}
